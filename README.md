# 🔥 UAS Pemograman Web

### 🔥 Create a portfolio website
- This Website Using HTML, CSS, JS, & Codeigniter3.
- With hover effect animation.
- With smooth animation

### 🔥 Installation Instructions
1. Download this repository 
2. Extract to local htdocs 
3. Run Xampp Server 
4. type this in your browser localhost/Landingpage 
5. Website can be run

### 🔥 To visit this website click the link below
https://ryankamil.github.io/website-portfolio2/

### 🔥 Screen recording of this website
https://www.youtube.com/watch?v=YIYGdAeB21Q


![Portfolio _ Bro2saur](https://user-images.githubusercontent.com/74499556/179805786-cfdbd5b6-80cf-4114-8b58-4746288aab9f.png)


"💙Coding but pusing"
