<!-- ++++ WORK ++++ -->
<section class="work section" id="work">
    <h2 class="section-title">Galery</h2>

    <div class="work_container bd-grid">
        <div class="work_img">
            <img src="assets/img/galery1.png" alt="" />
        </div>
        <div class="work_img">
            <img src="assets/img/galery2.png" alt="" />
        </div>
        <div class="work_img">
            <img src="assets/img/galery3.png" alt="" />
        </div>
        <div class="work_img">
            <img src="assets/img/galery4.png" alt="" />
        </div>
        <div class="work_img">
            <img src="assets/img/galery5.png" alt="" />
        </div>
        <div class="work_img">
            <img src="assets/img/galery6.png" alt="" />
        </div>
    </div>