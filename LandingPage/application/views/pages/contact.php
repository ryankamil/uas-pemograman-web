    <!--  CONTACT  -->
    <section class="contact section" id="contact">
        <h2 class="section-title">Contact</h2>
        <div class="contact_container bd-grid">
            <form action="" class="contact_form">
                <input type="text" placeholder="Nama" class="contact_input" />
                <input type="text" placeholder="Email" class="contact_input" />
                <textarea name="" id="" cols="0" rows="10" class="contact_input" placeholder="Masukan Pesan Anda"></textarea>
                <button class="contact_button button">Kirim</button>
            </form>
        </div>
    </section>
    </main>