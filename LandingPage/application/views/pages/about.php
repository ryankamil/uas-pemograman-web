<!-- section about -->

<section class="about section" id="about">
    <h2 class="section-title">About</h2>
    <div class="about_container bd-grid">
        <div class="about_img">
            <img src="assets/img/me.jpg" alt="" />
        </div>

        <div>
            <h2 class="about_subtitle">Halo, <br> I'am Muhammad Ryan Kamil</h2>
            <p class="about_text">
                STMIK WIDURI Informatics Engineering student.<br>
                I have a hobby of growing ornamental plants and graphic design.</p>
        </div>
    </div>
</section>