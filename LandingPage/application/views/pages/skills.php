 <!-- +++ SKILLS +++ -->
 <section class="skills section" id="skills">
     <h2 class="section-title">Skills</h2>
     <div class="skills_container bd-grid">
         <div>
             <h2 class="skills_subtitle">Profesional Skilss</h2>
             <p class="skills_text">This is a collection of abilities that I learned so far</p>

             <div class="skills_data">
                 <div class="skills_names">
                     <i class="fa-solid fa-code skills_icon"></i>
                     <span class="skill_name">Coding</span>
                 </div>
                 <div>
                     <span class="skills_percentage">20%</span>
                 </div>
                 <div class="skills_bar skills_html"></div>
             </div>

             <div class="skills_data">
                 <div class="skills_names">
                     <i class="fa-solid fa-pen skills_icon"></i>
                     <span class="skill_name">Write</span>
                 </div>
                 <div>
                     <span class="skills_percentage">20%</span>
                 </div>
                 <div class="skills_bar skills_css"></div>
             </div>

             <div class="skills_data">
                 <div class="skills_names">
                     <!-- <i class="fa-brands fa-js-square skills_icon"></i> -->
                     <i class="fa-solid fa-plant-wilt skills_icon"></i>
                     <span class="skill_name">Gardening</span>
                 </div>
                 <div>
                     <span class="skills_percentage">40%</span>
                 </div>
                 <div class="skills_bar skills_js"></div>
             </div>

             <div class="skills_data">
                 <div class="skills_names">
                     <i class="fa-solid fa-palette skills_icon"></i>
                     <span class="skill_name">Graphic Design</span>
                 </div>
                 <div>
                     <span class="skills_percentage">30%</span>
                 </div>
                 <div class="skills_bar skills_ui"></div>
             </div>
         </div>
         <div>
             <img src="assets/img/skills.png" alt="" />
         </div>
     </div>
 </section>