<!doctype html>
<html lang="en">

<head>
    <!-- Header -->
    <?php $this->load->view('_partials/head.php'); ?>
</head>

<body>
    <!-- Navbar -->
    <?php $this->load->view('_partials/navbar.php'); ?>

    <!-- Content -->
    <?php $this->load->view('pages/home.php') ?>

    <!-- section about -->
    <?php $this->load->view('pages/about.php') ?>

    <!-- section skills -->
    <?php $this->load->view('pages/skills.php') ?>

    <!-- section galery -->
    <?php $this->load->view('pages/galery.php') ?>

    <!-- section contact -->
    <?php $this->load->view('pages/contact.php') ?>

    <!-- Footer -->
    <?php $this->load->view('_partials/footer.php') ?>
</body>

</html>