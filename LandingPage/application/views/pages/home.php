<main class="i-main">
    <!-- Home -->
    <section class="home bd-grid" id="home">
        <div class="home_data">
            <h1 class="home_title">
                Hi, <br />
                I'am <span class="home_title-color">Ryan</span> <br />
                <span class="auto-type"></span>
            </h1>
            <a href="#" class="button">Contact</a>
        </div>

        <div class="home_social">
            <a href="#" class="home_social-icon">
                <i class="fab fa-linkedin-in"></i>
            </a>
            <a href="https://gitlab.com/ryankamil" class="home_social-icon">
                <i class="fab fa-gitlab"></i>
            </a>
            <a href="https://github.com/ryankamil" class="home_social-icon">
                <i class="fab fa-github"></i>
            </a>
        </div>

        <div class="home_img">
            <img src="assets/img/undraw6.png" alt="" />
        </div>
    </section>