<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Portfolio | Bro2saur</title>
<!-- linked to external css -->
<link rel="stylesheet" href="assets/css/style3.css">

<!-- icon logo tab -->
<link rel="shortcut icon" href="assets/img/icon.png">

<!--font awesome icon-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css" />