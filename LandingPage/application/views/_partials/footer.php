<!-- +++ FOOTER  SECTION ++++ -->
<footer class="footer">
    <!-- <p class="footer_title">Ryan</p> -->
    <div class="footer_social">
        <a href="#" class="footer_icon"><i class="fab fa-facebook-f"></i></a>
        <a href="#" class="footer_icon"><i class="fab fa-instagram"></i></a>
        <a href="#" class="footer_icon"><i class="fab fa-twitter"></i></a>
    </div>
    <p>&#169; 2022 inspired by Bedimcode</p>
</footer>

<!-- Scroll Reveal -->
<script src="https://unpkg.com/scrollreveal"></script>

<!-- Auto Type -->
<script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.12"></script>

<!-- linked to external script -->
<script src="assets/js/main3.js"></script>